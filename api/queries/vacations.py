from pydantic import BaseModel
from typing import Optional, Union, List
from datetime import date
from queries.pool import pool

class Error(BaseModel):
  message: str

class VacationIn(BaseModel):
  name: str
  from_date: date
  to_date: date
  thoughts: Optional[str]

class VacationOut(VacationIn):
  id: int

class VacationRepository:
  def vacation_in_to_out(self, id: int, vacation: VacationIn) -> VacationOut:
    return VacationOut(id=id,**vacation.dict())

  def record_to_vacation_out(self,record):
    return (
      VacationOut(
        id=record[0],
        name=record[1],
        from_date=record[2],
        to_date=record[3],
        thoughts=record[4],
      )
    )

  def try_db(self, execute, result_func, error_return):
    try:
      with pool.connection() as conn:
        with conn.cursor() as db:
          return result_func(db.execute(*execute))
    except Exception as e:
      print(e)
      return error_return

  def get_one(self,vacation_id: int) -> Optional[VacationOut]:
    execute=[
    """
    SELECT id
      , name
      , from_date
      , to_date
      , thoughts
    FROM vacations
    WHERE id = %s
    """,
    [vacation_id]
    ]

    def result_func(result):
      record = result.fetchone()
      if record is None:
        return None
      return self.record_to_vacation_out(record)

    return self.try_db(
      execute,
      result_func,
      {'message': "Could not get the vacations"}
    )


  def delete(self, vacation_id: int) -> bool:
    execute=[
    f"""
    DELETE FROM vacations
    WHERE id = {vacation_id}
    """
    ]

    def result_func(result):
      return True

    return self.try_db(execute,result_func,False)


  def update(self, vacation_id: int, vacation: VacationIn) -> Union[VacationOut, Error]:
    execute=["""
      UPDATE vacations
      SET name = %s
        , from_date = %s
        , to_date = %s
        , thoughts = %s
      WHERE id = %s
      """,
      [
        vacation.name,
        vacation.from_date,
        vacation.to_date,
        vacation.thoughts,
        vacation_id
      ]
    ]

    def result_func(result):
      return self.vacation_in_to_out(vacation_id,vacation)

    error = {'message': "Could not update the vacation"}

    return self.try_db(
      execute,
      result_func,
      error
    )


  def get_all(self) -> Union[ Error, List[VacationOut]]:
    execute=["""
    SELECT id, name, from_date, to_date, thoughts
    FROM vacations
    ORDER by id;
    """
    ]
    def result_func(result):
      return [ self.record_to_vacation_out(record) for record in result ]
    error={'message': "Could not get all vacations"}
    return self.try_db(
      execute,
      result_func,
      error
    )


  def create(self,vacation: VacationIn) -> VacationOut:
    execute=[
      """
      INSERT INTO vacations
        (name, from_date, to_date, thoughts)
      VALUES
        (%s, %s, %s, %s)
      RETURNING id;
      """,
      [
        vacation.name,
        vacation.from_date,
        vacation.to_date,
        vacation.thoughts
      ]
    ]
    def result_func(result):
      id = result.fetchone()[0]
      return self.vacation_in_to_out(id,vacation)
    error={'message': "Could not create vacation"}

    return self.try_db(
      execute,
      result_func,
      error
    )
